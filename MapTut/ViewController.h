//
//  ViewController.h
//  MapTut
//
//  Created by Noel Hwande on 6/2/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *myMapView;

@end
