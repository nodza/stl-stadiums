//
//  ViewController.m
//  MapTut
//
//  Created by Noel Hwande on 6/2/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import "ViewController.h"
#import "Annotation.h"

@interface ViewController ()

@end

// Busch Stadium Coordinates
#define BUSCH_LATITUDE 38.622585;
#define BUSCH_LONGITUDE -90.192816;

#define RAMS_LATITUDE 38.632657; 
#define RAMS_LONGITUDE -90.18721;

#define BLUES_LATITUDE 38.626175;
#define BLUES_LONGITUDE -90.202131;

// Span
#define THE_SPAN 0.05f;

@implementation ViewController
@synthesize myMapView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Create the region
    MKCoordinateRegion myRegion;
    
    // Create center
    CLLocationCoordinate2D center;
    center.latitude = BUSCH_LATITUDE;
    center.longitude = BUSCH_LONGITUDE;
    
    // Create spans
    MKCoordinateSpan span;
    span.latitudeDelta = THE_SPAN;
    span.longitudeDelta = THE_SPAN;
    
    // Set region center
    myRegion.center = center;
    myRegion.span = span;
    
    // Set mapView
    [myMapView setRegion:myRegion animated:YES];
    
    // Annotation
    NSMutableArray * locations = [[NSMutableArray alloc] init];
    CLLocationCoordinate2D location;
    Annotation * myAnn;
    
    // Busch Annotation
    myAnn = [[Annotation alloc] init];
    location.latitude = BUSCH_LATITUDE;
    location.longitude = BUSCH_LONGITUDE;
    myAnn.coordinate = location;
    myAnn.title = @"Busch Stadium";
    myAnn.subtitle = @"Home of the Relentless RedBirds";
    [locations addObject:myAnn];
    
    // Edward Jones Dome Annotation
    myAnn = [[Annotation alloc] init];
    location.latitude = RAMS_LATITUDE;
    location.longitude = RAMS_LONGITUDE;
    myAnn.coordinate = location;
    myAnn.title = @"Edward Jones Dome";
    myAnn.subtitle = @"The Greatest Show on Turf";
    [locations addObject:myAnn];
    
    // Scottrade Center Annotation
    myAnn = [[Annotation alloc] init];
    location.latitude = BLUES_LATITUDE;
    location.longitude = BLUES_LONGITUDE;
    myAnn.coordinate = location;
    myAnn.title = @"Scottrade Center";
    myAnn.subtitle = @"Go Blues!";
    [locations addObject:myAnn];
    
    // 1. Create a coordinate for annotation
//    CLLocationCoordinate2D buschLocation;
//    buschLocation.latitude = BUSCH_LATITUDE;
//    buschLocation.longitude = BUSCH_LONGITUDE;
    
//    Annotation * myAnnotation = [Annotation alloc];
//    myAnnotation.coordinate = buschLocation;
//    myAnnotation.title = @"Busch Stadium";
//    myAnnotation.subtitle = @"Home of the Relentless RedBirds";
//    
    [self.myMapView addAnnotations:locations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
