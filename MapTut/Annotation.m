//
//  Annotation.m
//  MapTut
//
//  Created by Noel Hwande on 6/2/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation
@synthesize coordinate, title, subtitle;

@end
