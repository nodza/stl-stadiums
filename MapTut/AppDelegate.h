//
//  AppDelegate.h
//  MapTut
//
//  Created by Noel Hwande on 6/2/13.
//  Copyright (c) 2013 Noel Hwande. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
